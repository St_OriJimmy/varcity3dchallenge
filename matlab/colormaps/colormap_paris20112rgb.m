function [cm, labels_str, labels_unused] = colormap_paris20112rgb

labels_str = {'void','window','wall','balcony','door','roof','sky','shop'};
labels_unused = [1];

% NAME:     Void Window Wall Balcony Door Roof Sky Shop ROAD
% LABELMAP  0  1       2       3      4      5   6   7     8
% MATLAB    1  2       3       4      5      6   7   8     9

         
% -1 Outlier	0	0	0         
% 0	Window	255	0	0
% 1	Wall	255	255	0
% 2	Balcony	128	0	255
% 3	Door	255	128	0
% 4	Roof	0	0	255
% 5	Sky	128	255	255
% 6	Shop	0	255	0
% % 7	Road	128	128	128

cm = [
0	0	0	;
255	0	0	;
255	255	0	;
128	0	255	;
255	128	0	;
0	0	255	;
128	255	255	;
0	255	0	; 
%128	128	128	; 
]/255;

