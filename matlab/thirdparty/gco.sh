
# download
wget http://vision.csd.uwo.ca/code/gco-v3.0.zip

# extract
mkdir gco
cd gco
unzip ../gco-v3.0.zip

# compile
cd matlab
matlab -nodesktop -r 'GCO_BuildLib'

exit
